# Skript zum Parsen von blastn output (outfmt 7)

import sys

def blastn_parser(path):

    with open(path, "r") as datei:
        prev_line = " "
        contig_taxid_list = []
        combi = []
        result_list = []
        for line in datei:
            if prev_line[0] == "#" and line[0] != "#":
                contig = line.split("\t")[0].split("_")[-1]
                taxid = int(line.split("\t")[2].split(";")[0]) 
                combi = [contig, taxid]

            if prev_line[0] != "#" and line[0] != "#":
                taxid = int(line.split("\t")[2].split(";")[0])
                if taxid not in combi:
                    combi.append(taxid)
            if prev_line[0] == "a" and line[0] == "#":
                result_list.append(combi)
                combi = []
            prev_line = line
        
    return result_list

def format_taxids(taxid_list, path):
    with open(f"/home/paddy/2021_09_thuenen_institut/taxonkit_in/{path.split('/')[-1].split('.')[0]}_taxonkit_input.txt", "w") as datei:
        for contig in range(len(taxid_list)):
            datei.write(f"{taxid_list[contig][0]}\n")
            for i in range(1, len(taxid_list[contig])):
                datei.write(f"{taxid_list[contig][i]}\n")
    return None



if __name__ == "__main__":

    taxid_list = blastn_parser(path =  sys.argv[1])
    format_taxids(taxid_list, path = sys.argv[1])
    
