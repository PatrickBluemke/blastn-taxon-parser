#!/bin/bash

filenames=`ls ../taxonkit_in/*`


for file in ${filenames}
do
echo ${file}
simplename=$(basename -- "${file}")
filename="${simplename%.*}"
singularity exec ~/singularity_containers/taxonkit%3A0.8.0--h9ee0642_0 taxonkit lineage ${file} \
 | singularity exec ~/singularity_containers/taxonkit%3A0.8.0--h9ee0642_0 taxonkit reformat -f '{k}|{p}|{s}' > ../taxonkit_out/${filename}_taxonkit_out.txt

done
