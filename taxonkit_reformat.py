#short skript to transform a specific taxonkit-output (contig-info, taxIDs, lineage, k/p/s)
# to an csv file of the form: contig-no., taxID, kingdom, phylum, species

import sys

input_file = sys.argv[1]
output_file = sys.argv[2]


with open(input_file, "r") as datei:
    in_out_list = []
    prev_line = " "
    for line in datei:
        if line[:6] == "contig" or line[:4]=="BHDV":
            current_Name = line.strip()
        else:
            current_taxid = line.split("\t")[0]
            current_kingdom = line.split("\t")[2].split("|")[0]
            if len(line.split("\t")[2].split("|")) > 1: 
                current_phylum = line.split("\t")[2].split("|")[1]
            else:
                current_pyhlum = "N/A"
            if len(line.split("\t")[2].split("|")) > 1: 
                current_species = line.split("\t")[2].split("|")[2]
            else:
                current_species = "N/A"

            content = f"{current_Name},{current_taxid},{current_kingdom},{current_phylum},{current_species}"
            in_out_list.append(content)
        
        prev_line = line



with open(output_file, "w") as datei:
    datei.write("Name,taxID,kingdom,phylum,species\n")
    for i in range(len(in_out_list)):
        datei.write(f"{in_out_list[i]}")

