#!/bin/bash

filenames=`ls ../output_blast_nt/*`

for file in ${filenames}
do
echo ${file}
simplename=$(basename -- "${file}")
filename="${simplename%.*}"
python3 blastn_output_parser.py ${file} 
done
